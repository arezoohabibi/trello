import { createUseStyles } from 'react-jss';

const useStyle = createUseStyles({
  backdrop: () => ({
    position: 'fixed',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: 'rgba(51, 51, 51, 0.3)',
    backdropFilter: ' blur(1px)',
    opacity: 0,
    transition: 'all 100ms cubic-bezier(0.4, 0, 0.2, 1)',
    transitionDelay: '200ms',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    '& .modal-content': {
      transform: 'translateY(100px)',
      transition: 'all 200ms cubic-bezier(0.4, 0, 0.2, 1)',
      opacity: 0,
    },

    '&.active': {
      transitionDuration: '250ms',
      transitionDelay: '0ms',
      opacity: 1,

      '& .modal-content': {
        transform: 'translateY(0)',
        opacity: 1,
        transitionDelay: '150ms',
        transitionDuration: '350ms',
      },
    },
  }),

  content: () => ({
    position: 'relative',
    padding: '20px',
    boxSizing: 'border-box',
    minHeight: '50px',
    minWidth: '50px',
    maxHeight: '80%',
    maxWidth: '80%',
    boxShadow: '0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23)',
    backgroundColor: 'white',
    borderRadius: '2px',
  }),
});

export default useStyle;
