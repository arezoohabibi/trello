import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import cx from '../../utils/class-names';
import Portal from '../portal';
import useStyle from './style';

export default function Modal({ open, onClose, locked, children }) {
  const classes = useStyle();
  const [active, setActive] = useState(false);
  const backdrop = useRef(null);

  useEffect(() => {
    const { current } = backdrop;

    const transitionEnd = () => setActive(open);

    const keyHandler = (e) =>
      !locked && [27].indexOf(e.which) >= 0 && onClose();

    const clickHandler = (e) => !locked && e.target === current && onClose();

    if (current) {
      current.addEventListener('transitionend', transitionEnd);
      current.addEventListener('click', clickHandler);
      window.addEventListener('keyup', keyHandler);
    }

    if (open) {
      window.setTimeout(() => {
        document.activeElement.blur();
        setActive(open);
        document.querySelector('#root').setAttribute('inert', 'true');
      }, 10);
    }

    return () => {
      if (current) {
        current.removeEventListener('transitionend', transitionEnd);
        current.removeEventListener('click', clickHandler);
      }

      document.querySelector('#root').removeAttribute('inert');
      window.removeEventListener('keyup', keyHandler);
    };
  }, [open, locked, onClose]);

  return (
    <div>
      {(open || active) && (
        <Portal className="modal-portal">
          <div
            onClick={onClose}
            ref={backdrop}
            className={cx(classes.backdrop, active && open && 'active')}
          >
            <div
              className={cx('modal-content', classes.content)}
              onClick={(e) => {
                e.stopPropagation();
              }}
            >
              {children}
            </div>
          </div>
        </Portal>
      )}
    </div>
  );
}

Modal.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func,
  locked: PropTypes.bool,
  open: PropTypes.bool,
};

Modal.defaultProps = {
  children: null,
  onClose: () => {},
  locked: false,
  open: false,
};
