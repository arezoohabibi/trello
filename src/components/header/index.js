import React from 'react';
import logo from '../../assets/images/logo.gif';
import useStyle from './style';

function Header() {
  const classes = useStyle();

  return (
    <header className={classes.header}>
      <img src={logo} alt="logo" />
    </header>
  );
}

export default Header;
