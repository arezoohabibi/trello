import { createUseStyles } from 'react-jss';

const useStyle = createUseStyles({
  header: () => ({
    backdropFilter: 'blur(6px)',
    maxHeight: '44px',
    padding: '11px 25px',
    backgroundColor: 'rgba(0, 0, 0, 0.45)',
    position: 'fixed',
    width: '100%',
    top: 0,
    '& img': {
      maxHeight: '17px',
    },
  }),
});

export default useStyle;
