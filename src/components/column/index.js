import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import Modal from '../modal';
import useStyle from './style';

function Column({ name, isLast, columnId, onUpdateCards, onMoveCardForward }) {
  const classes = useStyle();
  const editTextBox = useRef();
  const [addNewCardVisibility, toggleAddNewCardVisibility] = useState(false);
  const [cardValue, setCardValue] = useState('');
  const [cardList, setCardList] = useState([]);
  const [editModalData, setEditModalData] = React.useState({
    visibility: false,
    content: null,
    id: null,
  });

  const handleAddNewCard = () => {
    toggleAddNewCardVisibility(true);
  };

  const handleChangeCardValue = (event) => {
    setCardValue(event.target.value);
  };

  const handleSubmitCard = () => {
    setCardList((prev) => [
      ...prev,
      {
        id: prev.length + 1,
        content: cardValue,
      },
    ]);
    handleReset();
  };

  const handleReset = () => {
    setCardValue('');
    toggleAddNewCardVisibility(false);
  };

  const handleShowEditModal = (content, id) => {
    setEditModalData({
      visibility: true,
      content,
      id,
    });
  };

  const handleEditCardContent = () => {
    const newList = cardList.map((item) => {
      if (editModalData.id === item.id) {
        return {
          ...item,
          content: editTextBox.current.value,
        };
      }

      return item;
    });
    setCardList(newList);
    handleCloseEditModal();
  };

  const handleCloseEditModal = () => {
    setEditModalData({
      content: null,
      visibility: false,
      id: null,
    });
  };

  useEffect(() => {
    onUpdateCards({ cardList, columnId });
  }, [cardList]);

  const handleMoveForward = (e, cardData) => {
    e.stopPropagation();
    const filteredCardList = cardList.filter((item) => {
      return cardData.id !== item.id;
    });
    setCardList(filteredCardList);
    onMoveCardForward(columnId + 1, cardData);
  };

  return (
    <div className={classes.columnWrapper}>
      <div className={classes.column}>
        <h4 className={classes.name}>{name}</h4>
        {cardList.map((item) => (
          <div
            key={item.id}
            className={classes.card}
            onClick={() => handleShowEditModal(item.content, item.id)}
          >
            <span>{item.content}</span>
            {isLast === false && (
              <button type="button" onClick={(e) => handleMoveForward(e, item)}>
                {'>'}
              </button>
            )}
          </div>
        ))}
        {addNewCardVisibility ? (
          <div>
            <textarea
              rows="4"
              cols="50"
              className={classes.cardInput}
              onChange={handleChangeCardValue}
              value={cardValue}
            />
            <button type="button" onClick={handleSubmitCard}>
              Add Card
            </button>
            <button type="button" onClick={handleReset}>
              X
            </button>
          </div>
        ) : (
          <button type="button" onClick={handleAddNewCard}>
            + Add a card
          </button>
        )}
      </div>
      <Modal
        locked
        onClose={handleCloseEditModal}
        open={editModalData.visibility}
      >
        <input defaultValue={editModalData.content} ref={editTextBox} />
        <button type="button" onClick={handleEditCardContent}>
          edit
        </button>
      </Modal>
    </div>
  );
}

Column.propTypes = {
  name: PropTypes.string.isRequired,
  columnId: PropTypes.number.isRequired,
  isLast: PropTypes.bool.isRequired,
  onUpdateCards: PropTypes.func,
  onMoveCardForward: PropTypes.func,
};

Column.defaultProps = {
  onMoveCardForward: () => {},
  onUpdateCards: () => {},
};

export default Column;
