import { createUseStyles } from 'react-jss';

const useStyle = createUseStyles({
  columnWrapper: {
    boxSizing: 'border-box',
    display: 'inline-block',
    height: '100%',
    margin: '0 4px',
    verticalAlign: 'top',
    whiteSpace: 'nowrap',
    width: '272px',
  },
  column: () => ({
    padding: '5px',
    backgroundColor: '#ebecf0',
    borderRadius: '3px',
    boxSizing: 'borderBox',
    display: 'flex',
    flexDirection: 'column',
    maxHeight: '100%',
    position: 'relative',
    whiteSpace: 'normal',
    width: '100% !important',
  }),
  name: {
    margin: '0 0 10px',
  },
  cardInput: {
    width: '100%',
    boxSizing: 'border-box',
    marginBottom: '5px',
  },
  card: {
    backgroundColor: '#fff',
    borderRadius: '3px',
    boxShadow: '0 1px 0 #091e4240',
    cursor: 'pointer',
    display: 'flex',
    marginBottom: '8px',
    maxWidth: '300px',
    minHeight: '20px',
    position: 'relative',
    textDecoration: 'none',
    padding: '5px',
    justifyContent: 'space-between',
  },
  cardList: {
    flex: '1 1 auto',
    margin: '0 4px',
    minHeight: 0,
    overflowX: 'hidden',
    overflowY: 'auto',
    padding: '0 4px',
  },
});

export default useStyle;
