import React, { useState } from 'react';
import Column from './components/column';
import Header from './components/header';
import useStyle from './style';

function App() {
  const classes = useStyle();
  const [columnList, setColumnList] = useState([]);
  const [addNewColVisibility, toggleAddNewColVisibility] = useState(false);
  const [columnNameValue, setColumnNameValue] = useState('');

  const addList = () => {
    setColumnList((prev) => [
      ...prev,
      {
        name: columnNameValue,
        id: prev.length + 1,
        cardList: [],
      },
    ]);
    setColumnNameValue('');
    toggleAddNewColVisibility(false);
  };

  const handleAddList = () => {
    toggleAddNewColVisibility(true);
  };

  const handleHideColumnName = () => {
    toggleAddNewColVisibility(false);
  };

  const handleChangeColumnName = (event) => {
    setColumnNameValue(event.target.value);
  };

  const handleUpdateCardList = ({ cardList, columnId }) => {
    const newColList = columnList.map((item) => {
      if (columnId === item.id) {
        return {
          ...item,
          cardList,
        };
      }

      return item;
    });
    setColumnList(newColList);
  };

  const handleMoveCardForward = (targetColumnId, cardData) => {
    const targetColumnData = columnList.find((element) => {
      return element.id === targetColumnId;
    });
    targetColumnData.cardList.push(cardData);

    handleUpdateCardList({
      cardList: targetColumnData.cardList,
      columnId: targetColumnId,
    });
  };

  return (
    <main className={classes.main}>
      <Header />
      <div className={classes.container}>
        {columnList.map((item, index) => (
          <Column
            key={index}
            name={item.name}
            columnId={item.id}
            isLast={index + 1 === columnList.length}
            onMoveCardForward={handleMoveCardForward}
            onUpdateCards={handleUpdateCardList}
          />
        ))}
        <div className={classes.addListBox}>
          {addNewColVisibility ? (
            <>
              <div>
                <input
                  onChange={handleChangeColumnName}
                  value={columnNameValue}
                  type="text"
                  className={classes.columnNameInput}
                  placeholder="please write column name"
                />
              </div>
              <button
                type="button"
                onClick={addList}
                disabled={!columnNameValue}
              >
                Add list
              </button>
              <button type="button" onClick={handleHideColumnName}>
                X
              </button>
            </>
          ) : (
            <button
              type="button"
              className={classes.addColumnButton}
              onClick={handleAddList}
            >
              + Add another list
            </button>
          )}
        </div>
      </div>
    </main>
  );
}

export default App;
