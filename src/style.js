import { createUseStyles } from 'react-jss';

const useStyle = createUseStyles({
  main: {
    paddingTop: '44px',
  },
  container: () => ({
    padding: '20px',
    whiteSpace: 'nowrap',
    maxWidth: '100%',
  }),
  addListBox: {
    boxSizing: 'border-box',
    display: 'inline-block',
    height: '100%',
    margin: '0 4px',
    verticalAlign: 'top',
    width: '272px',
    maxWidth: '272px',
  },
  columnNameInput: {
    width: '100%',
    height: '28px',
    border: 'none medium',
    marginBottom: '4px',
  },
  addColumnButton: {
    width: '100%',
    backgroundColor: '#ffffff3d',
    borderRadius: '3px',
    height: 'auto',
    minHeight: '32px',
    color: '#fff',
    margin: '0 4px',
    fontSize: '14px',
    textAlign: 'left',
    padding: '6px 7px',
    cursor: 'pointer',
    border: 'none medium',
    transition:
      'background 85ms ease-in,opacity 40ms ease-in,border-color 85ms ease-in',
    '&:hover': {
      backgroundColor: '#ffffff52',
    },
    '&.hide': {
      display: 'none',
    },
  },
  addButton: {
    backgroundColor: '#ffffff3d',
    borderRadius: '3px',
    height: 'auto',
    minHeight: '32px',
    color: '#fff',
    margin: '0 4px',
    fontSize: '14px',
    textAlign: 'left',
    padding: '6px 7px',
    cursor: 'pointer',
    border: 'none medium',
    transition:
      'background 85ms ease-in,opacity 40ms ease-in,border-color 85ms ease-in',
    '&:hover': {
      backgroundColor: '#ffffff52',
    },
    '&.hide': {
      display: 'none',
    },
  },
});

export default useStyle;
